---
title: "Incertitudes"
author: "Guillaume Evin"
---

## Problématique

Pour étudier l'impact du changement climatique sur un « écosystème » donné (exemple un hydrosystème incluant bassins versants hydrologiques et ouvrages de gestion de la ressource en eau), on utilise classiquement les sorties d'un **ensemble de chaines de simulation**. Chacune de ces chaines associe en chaine différents types de modèles, typiquement.

1.  **un modèle du système terre** permettant de simuler l'évolution du climat à l'échelle du globe pour un scénario d'émission de gaz à effet de serre donné (*General Circulation Model* ou GCM),
2.  **un modèle de climat régional**, permettant de simuler, sur la base des sorties du modèle précédent, l'évolution du climat régional pour un domaine spatial réduit incluant l'écosystème cible (*Regional Climate Model* ou *RCM*) et
3.  **un modèle d'impact** (e.g. modèle intégré de la ressource en eau), permettant de simuler la réponse de l'écosystème cible aux changements du climat régional à partir des sorties du modèle de climat régional.

Si l'on s'intéresse par exemple à l'évolution du régime hydrologique d'un cours d'eau donné, le modèle d'impact sera un modèle hydrologique.

Dans l'étude d'impact, une question critique est **l'estimation de l'évolution à long terme** du comportement du système étudié - **la réponse climatique** - ainsi que **l'estimation des incertitudes associées** à cette évolution estimée. Caractériser la réponse climatique et les incertitudes permet de fournir des informations cruciales pour anticiper les adaptations à envisager. Les incertitudes sont de diverses natures. Elles incluent

-   **les incertitudes associées au scénario d'émission** ; ces incertitudes sont évaluées de façon partielle en considérant différents scénarios d'émission -- qui correspondent chacun à différentes trajectoires politiques et socio-économiques. Ce sont par exemple les scénarios RCP (*Representative Concentration Pathways*) ou les plus récent scénarios SSP (*Shared Socioeconomic Pathways*).
-   **les incertitudes modèles** ; Ces incertitudes résultent de l'imperfection des modèles de simulation. Elles se traduisent, pour un forçage donné, par des réponses climatiques différentes selon le modèle considéré. Par exemple, différents Modèles de Climat Globaux (GCMs), forcé par un même scénario d'émission, donnent des trajectoires de réchauffement différentes. Différents Modèles de Climat Régionaux (RCMs), forcé par un même GCM, donnent des climats régionaux différents, etc.
-   **les incertitudes liées à la variabilité interne du climat**; Ces incertitudes résultent de la nature chaotique du climat, qui indépendamment des forçages anthropiques actuels, fluctue naturellement. Dans les expériences climatiques globales d'un GCM donné, la variabilité interne du climat se traduit par exemple par des fluctuations basses fréquences du climat autour du signal de changement (autour de la réponse climatique du modèle).

# Concepts

L'objectif du présent TP est de comprendre les étapes et calculs nécessaires pour 1/ estimer l'évolution long terme espérée d'une variable climatique donnée et 2/ caractériser les différentes sources d'incertitudes associées à un ensemble multimodèles (MME) de projections climatiques régionales.

Le jeu de données synthétique permet d'illustrer ce que l'on appelle les "réponses climatiques" d'une chaine de simulation donnée ainsi que ce que l'on nomme l'"effet principal" d'un facteur d'incertitude (par example d'un modèle) particulier.

Plusieurs méthodes statistiques peuvent être utilisées pour 1) estimer les effets principaux des différents modèles/facteurs pris en compte dans l'ensemble de projections disponibles et 2) quantifier in fine les sources d'incertitudes de l'ensemble.

## Génération de données

Nous commencons par générer un ensemble fictif de réponses climatiques. Pour plusieurs combinaisons GCM/RCM obtenues avec un Modèle Climatique Régional (RCM) forcé par un Modèle Climatique Global (GCM, i.e. Modèle du système Terre), nous considérons que plusieurs projections (*runs*) sont disponibles. Nous connaissons donc la vraie moyenne d'ensemble et les vrais effets des modèles climatiques.

On considère que les données disponibles sont pour chaque expérience le réchauffement attendu en °C entre la période future P1 = 2070-2090 et la période dite de contrôle P0 = 1990-2010.

```{r}
# Nettoyer l'espace de travail
rm(list=ls()) 

# Propriétés de l'ensemble multi-modèle (multi-model ensemble, MME): à modifier pour tests
nRun = 100 # nombre des projections pour chaque combinaison GCM/RCM
sdRes = 1 # Ecart-type des résidus
nGCM = 4 # nombre de GCMs
nRCM = 3 # nombre de RCMs

# Nombre de projections climatique dans le MME: pour chaque GCM et chaque RCM, on considère "nRun" runs
nMME = nRun*nGCM*nRCM

# moyenne d'ensemble et effets
grandMean = 10 # moyenne d'ensemble (Grand Ensemble Mean)  
RCMeff = c(1,-3,2) # effets principaux des RCMs
GCMeff = c(4,1,-2,-3)  # effets principaux des GCMs

# initialisatiob des vecteurs
CR = tagRCM = tagGCM = vector(length = nMME)

# Construction d'un jeu de données de réponses climatiques pour différents combinaisons de GCM/RCM
# On génère des bruits d'une variable gaussienne 
k = 1
for(iRun in 1:nRun){
  for(iGCM in 1:nGCM){
    for(iRCM in 1:nRCM){
      # la réponse climatique est la somme de la moyenne d'ensemble, des effets principaux et du résidu
      CR[k]= grandMean + RCMeff[iRCM] + GCMeff[iGCM] + rnorm(n=1, mean=0, sd=sdRes)
      tagRCM[k] = paste0("RCM",iRCM)
      tagGCM[k] = paste0("GCM",iGCM)
      k = k+1
    }
  }
}

# Eventuellement, on peut considérer que les projections climatiques ne sont pas disponibles (elles n'ont pas été produite) pour une partie de l'ensemble. On attribue *NA* à une partie de l'ensemble, de manière aléatoire 
# nNA = round(nMME*0.1)
# CR[sample(1:nMME,nNA)] = NA

df.MME=data.frame(CR=CR,GCM=tagGCM,RCM=tagRCM)
```

## Estimation des effets de chaque RCM et GCM: Première approche

Nous voulons maintenant estimer "l'effet principal" de chaque RCM et "l'effet principal" de chaque GCM. Dans cette première, on considère des estimations directes par différence de moyenne globale et moyennes par type de RCM/GCM.

La projection pour la combinaison RCM_i/GCM_j peut s'écrire de la manière suivante:

$$\phi_{i,j} = \mu + \alpha[i] + \beta[j] + \epsilon$$

où $\mu$ représente la moyenne d'ensemble, $\alpha$ représente l'effet RCM, $\beta$ l'effet GCM et $\epsilon$ représente un bruit aléatoire par exemple dû à la variabilité naturelle du climat, centré sur zéro.

On peut visualiser facilement ces effets avec des boxplots.

```{r}
boxplot(CR~GCM, data=df.MME)
boxplot(CR~RCM, data=df.MME)
```

Déterminer MU, la moyenne d'ensemble des données. Afficher MU

```{r}
MU.est = mean(CR)
MU.est
```

Estimer la réponse climatique moyenne de toutes les expériences GCM/RCM qui ont comme RCM le RCM3. Estimer ensuite l'effet principal du "RCM3".

```{r}
CR.RCM3.est = mean(CR[tagRCM=="RCM3"])
RCM3eff.est = CR.RCM3.est - MU.est
```

Estimer l'effet principal de chaque RCM.

```{r}
# aggregate peut être utilisée pour calculer des statistiques par valeur d'une variable
# discrête (type de RCM/GCM)
aggByRCM = aggregate(df.MME$CR, list(df.MME$RCM), mean)
aggByRCM
CR.RCM.est = aggByRCM$x
RCMeff.est = CR.RCM.est - MU.est
RCMeff.est
```

Estimer l'effet principal de chaque GCM.

```{r}
aggByGCM = aggregate(df.MME$CR, list(df.MME$GCM), mean)
aggByGCM
CR.GCM.est = aggByGCM$x
GCMeff.est = CR.GCM.est - MU.est
GCMeff.est
```

A l'aide des effets principaux estimés pour le RCM3 et le GCM2, estimer la projection attendue pour la combinaison GCM2/RCM3.

```{r}
CR.RCM3.GCM2 = MU.est + GCMeff.est[2] + RCMeff.est[3]
```

On peut refaire les estimations précédentes sur d'autres jeux de données.

## Estimation des effets de chaque RCM et GCM: Deuxième approche

Dans cette deuxième approche, nous calons un modèle de régression linéaire au jeu de données en considérant les "GCM" et "RCM" en facteurs (les variables explicatives du modèle linéaire sont de type "catégorielles" et non "numériques").

La commande *lm* permet de spécifier et appliquer un modèle de régression linéaire.

-   L'argument *contrasts* permet de spécifier les contraintes sur les paramètres. Une contrainte classique est de considérer que leur somme est égale à 0. C'est celle que l'on spécifie ici.
-   Les coefficients sont rangés dans l'ordre, pour les RCMs puis pour les GCMs.
-   Le dernier effet des RCMs (RCM3) et des GCMs (GCM4) n'est pas donné par la fonction mais peut être obtenu facilement.

```{r}
lm.out = lm(CR~RCM+GCM,data = df.MME,contrasts=list(GCM=contr.sum,RCM=contr.sum))
lm.out

# effets RCM
RCMeff.est.partial = lm.out$coefficients[lm.out$assign==1]
RCMeff.est = c(RCMeff.est.partial,-sum(RCMeff.est.partial))

# effets GCM
GCMeff.est.partial = lm.out$coefficients[lm.out$assign==2]
GCMeff.est = c(GCMeff.est.partial,-sum(GCMeff.est.partial))
```

## ANalysis Of VAriance (ANOVA)

[Principe de l'ANOVA](https://statistique-et-logiciel-r.com/anova-a-2-facteurs-principe/)

Le principe de l'ANOVA consiste à décomposer **la dispersion totale des données**, en différentes origines :

-   la part imputable aux différents facteurs (RCM, GCM),
-   la part non expliquée, ou résiduelle.

Une composante classique de l'ANOVA correspond aux interactions entre les facteurs (GCM/RCM) qu'il est possible de quantifier si plusieurs projections sont disponibles pour chaque combinaison. On ignore cette composante ici (il faudrait générer des effets pour chaque combinaison GCM/RCM).

La variance totale des réponses climatiques est la somme des variances de l'ANOVA.

$$Var(\phi) = Var(\alpha) + Var(\beta) + Var[\epsilon]$$

```{r}
# Composantes de l'incertitude totale (Variance)
VarRCM = mean(RCMeff.est^2)  # incertitude RCM
VarGCM = mean((GCMeff.est^2))  # incertitude GCM
VarRes = mean((lm.out$residuals^2))  # Variabilité résiduelle
VarTot = mean((CR-mean(CR))^2) # Variance totale des réponses climatiques

# différentes sources d'incertitudes, en pourcentage
vecDecomp = c(VarRCM,VarGCM,VarRes)/VarTot

# on peut vérifier que la décomposition des écarts aux carrés est bien totale
sum(vecDecomp)
```

Les analyses de variance donnent un cadre permettant de tester si les effets sont signicatifs, avec l'application de tests statistiques.

```{r}
anova(lm.out)
```

# Partition des sources d'incertitude dans un ensemble multimodèle de projections climatiques

## Contexte

Dans les MMEs issus de modèles régionaux, il n'y a souvent qu'une simulation disponible pour chaque combinaison de modèles climatiques. Afin de séparer le signal lié à la réponse climatique de la variabilité interne, une solution possible est de considérer que la réponse climatique d'une chaîne correspond à sa composante basse fréquence. Cette variation doit être graduelle et lisse, la variabilité à plus haute fréquence des séries temporelles correspondant à la variabilité interne. Cette hypothèse de quasi-ergodicité a conduit à la méthode QE-ANOVA proposée par Hingray et Saïd (2014). Dans ce TP, des signaux de basse-fréquence sont extraits à l'aide de splines cubiques.

De plus, la plupart des ensembles de projections climatiques explorent de manière incomplète les combinaisons possibles. Cela peut poser des problèmes pour la partition des incertitudes. Les estimations directes basées sur des moyennes directes par facteur sont par exemple généralement biaisés, en raison par exemple de modèles RCM/GCM sur-représentés. L'application d'un modèle linéaire (fonction *lm*) ne souffre pas de ces limitations. De manière alternative, l'approche décrite dans Evin et al. (2019) utilise des méthodes bayésiennes traitant explicitement les données manquantes (les données manquantes font partie de l'inférence, approche appelée *data augmentation*). Cela permet de plus de propager l'incertitude due aux projections absentes dans l'estimation des effets du modèle ANOVA.

## Objectifs du TP

Ce TP vise à appliquer les fonctionnalités du package *QUALYPSO* (disponible sur CRAN) pour appliquer, illustrer et discuter les étapes suivantes:

-   Extraction de la réponse climatique.
-   Calcul des différences absolues et relatives par rapport à un horizon de référence.
-   Décomposition des incertitudes avec une méthode ANOVA.

Le package *QUALYPSO* permet également de présenter ces analyses en fonction de différents niveaux de réchauffement, au lieu d'horizons temporels (conf rapport spécial du GIEC sur les conséquences d'un réchauffement planétaire de 1,5 °C).

## Lecture des données

Le package *QUALYPSO* contient un ensemble de projections climatiques de moyennes de température en hiver (DJF) pour une région couvrant la plus grand partie de l'Europe continentale (région SREX Central-Eastern Europe "CEU"). Cet ensemble contient des projections pour la période 1971-2099 (129 années) pour 20 combinaisons de modèles GCMs (CMIP5) et RCMs, obtenues avec le scénario RCP8.5.

```{r}
# Nettoyer l'espace de travail
rm(list=ls()) 

# Si ce n'est pas déjà fait, installer le package QUALYPSO (connection internet requise)
# install.packages("QUALYPSO")

# Charger le package
library(QUALYPSO)

# Les projections de moyennes de température hivernale pour 20 simulations de 129 années sont contenus dans Y sous la forme d'une matrice 20 x 129
?Y
dim(Y)

# Les années correspondantes sont contenues dans le vecteur X_time_vec
X_time_vec

# Les modèles GCM et RCM correspondants aux 20 simulations sont données dans scenAvail (data.frame avec deux colonnes "GCM" et "RCM" et 20 lignes correspondant aux 20 simulations). Ces 20 simulations correspondent à 4 GCMs descendus en échelle aveec 5 RCMs
dim(scenAvail)
scenAvail
apply(scenAvail,2,unique)
```

## Exploration des données

```{r}
# Première représentation des simulations: superposition des différentes simulations avec une couleur par GCM
plot(-1,-1,xlim=range(X_time_vec),ylim=range(Y),xlab="Années",ylab="Température (°C)")
for(i in 1:nrow(Y)){
  lines(X_time_vec,Y[i,],col=i)
}

# La variabilité inter-annuelle est importante. On regarde souvent les moyennes sur 30 ans pour obtenir des statistiques du climat sur des fenêtres glissantes et gommer une partie de cette variabilité haute-fréquence
sizeWindow = 30
nYmean = 100
vecYmean = vector(length=nYmean)
plot(-1,-1,xlim=range(X_time_vec),ylim=range(Y),xlab="Années",ylab="Température (°C)")
for(i in 1:nrow(Y)){
  for(y in 1:nYmean){
    vecYmean[y] = mean(Y[i,y:(y+sizeWindow-1)])
  }
  lines(X_time_vec[1:nYmean]+sizeWindow/2,vecYmean,col=i)
}
```

## Extraction des réponses climatiques

La figure précédent montre clairement les différences importantes des températures projetées, même pour la période historique, de l'ordre de plusieurs degrés, sans que cela semble être une conséquence de la variabilité interne. L'extraction de la réponse climatique avec un modèle statistique permet de séparer le signal climatique de la variabilité interne et de travailler avec des différences relatives ou absolues par rapport à une période de référence. On suppose donc qu'on peut interpréter les évolutions simulées par les modèles de climat, et pas leurs valeurs absolues.

La fonction *smooth.spline* permet d'ajuster des splines cubiques à une série de données afin d'interpoler ou de lisser ces données. Cette approche est une alternative puissance à l'application polynomiale puisqu'on ne suppose pas *a priori* la forme de l'évolution et qu'on peut choisir facilement le degré de lissage à l'aide l'argument **spar** dans *R*. Pour *spar*=0.01, on a presque un interpolateur exact de la série alors que pour *spar*=1, on s'approche de l'ajustement d'une régression linéaire.

```{r}
# illustration pour la première simulation: peut être modifié
iSimu=1
# sPar peut varier entre 0 et l'infini
vec_sPar = c(0.01,0.5,1)

plot(-1,-1,xlim=range(X_time_vec),ylim=range(Y[iSimu,]),xlab="Années",ylab="Température (°C)")
lines(X_time_vec,Y[iSimu,],col="black")
for(i in 1:3){
  ySmooth = smooth.spline(x = X_time_vec,y = Y[iSimu,], spar=vec_sPar[i])$y
  lines(X_time_vec,ySmooth,col="red",lty=i,lwd=2)
}
legend("bottomright",legend = vec_sPar,lty=1:3,col="red",title="spar")
```

## Prise en main du package QUALYPSO

La fonction principale du package \textit{QUALYPSO} porte le même nom et effectue les traitements d'extraction de la réponse climatique, de calcul des différences absolues ou relatives par rapport à un horizon de référence et la décomposition des incertitudes avec une méthode ANOVA. Les deux seuls arguments requis sont:

-   **Y**: la matrice nS x nY des projections climatiques.
-   **scenAvail**: *data.frame* nS x nEff avec les *nEff* caractéristiques (par ex. le type de GCM) pour chacune des *nS* simulations.

Les autres arguments sont:

-   **X**: prédicteurs correspondants aux simulations, sous forme d'un vecteur ou de matrice. Ici, *X_time_vec* est le prédicteur temporel.
-   **Xfut**: valeur du prédicteur pour lequel on veut obtenir la décomposition ANOVA. Dans le package, un vecteur *Xfut_time* est fourni à titre d'exemple. La première valeur défini l'horizon de référence.
-   **listOption**: liste d'options supplémentaires. Par exemple *spar* peut être spécifié ici. Le choix d'avoir des différences absolues ou relative est spécifié avec *typeChangeVariable* qui peut valoir "abs" ou "rel" respectivement.
-   **ANOVAmethod**: par défaut, un modèle linéaire *lm* est appliqué pour obtenir la décomposition ANOVA.

```{r}
# list of options
listOption = list(typeChangeVariable='rel',spar=1)

# call QUALYPSO
QUALYPSO.time = QUALYPSO(Y=Y,scenAvail=scenAvail,X=X_time_vec,
                         Xfut=Xfut_time,listOption=listOption)
```

L'objet retourné par la fonction *QUALYPSO* est une liste contenant toutes les estimations et les informations sur la décomposition des incertitudes. Nous pouvons par exemple obtenir les informations sur les réponses au changement climatique (en différence absolue) extraites des projections dans `QUALYPSO.time$CLIMATEESPONSE$phiStar`.

```{r}
phiStar =  QUALYPSO.time$CLIMATERESPONSE$phiStar
plot(-1,-1,xlim=range(Xfut_time),ylim=range(phiStar),xlab="Années",ylab="Température (°C)")
for(i in 1:nrow(Y)){
  lines(Xfut_time,phiStar[i,],col=i)
}

# on peut essayer de produire une figure similaire avec une couleur par GCM
vecGCM = unique(scenAvail$GCM)
plot(-1,-1,xlim=range(Xfut_time),ylim=range(phiStar),xlab="Années",ylab="Température (°C)")
for(i in 1:nrow(Y)){
  lines(Xfut_time,phiStar[i,],col=which(scenAvail$GCM[i]==vecGCM))
}
legend("bottomright",legend=vecGCM,col=1:4,lty=1)
```

On peut supposer un effect GCM, les projections obtenues avec le GCM HadGEM2-ES semble par exemple être plus "chaudes" et celles obtenues avec le GCM MPI-ESM-LR plus "froides". La décomposition ANOVA permet de quantifier ces effets.

La somme des effets étant pour chaque année, par construction, nulle, on peut interpréter les comportements des GCMs (ou des RCMs) relativement aux autres.

```{r}
# moyenne d'ensemble
plotQUALYPSOgrandmean(QUALYPSO.time)

# effet GCM: les projections obtenues avec le GCM HadGEM2-ES sont en moyenne 0.6°C plus chaudes que les autres projections
plotQUALYPSOeffect(QUALYPSO.time,nameEff = "GCM")

# effet RCM: de manière similaire, quels sont les RCMs plus "chauds/froids" que les autres?
plotQUALYPSOeffect(QUALYPSO.time,nameEff = "RCM")
```

Un des objectifs principaux de ces méthodes de décomposition d'incertitudes est de pouvoir apprécier l'importance de chacune des sources d'incertitude. La figure suivante montre la part relative de chacune de ces sources dans l'incertitude (variance) totale.

Quelle est la part due aux modèles climatiques? A la variabilité interne? A la variabilité résiduelle (limite du modèle ANOVA additif, non prise en compte des interactions)?

```{r}
# la part des différentes sources d'incertitude est contenue dans QUALYPSO.time$DECOMPVAR
QUALYPSO.time$DECOMPVAR

# la fonction plotQUALYPSOTotalVarianceDecomposition permet d'illustrer cette décomposition
plotQUALYPSOTotalVarianceDecomposition(QUALYPSO.time)

# la variabilité des réponses au changement climatique (écart-type) correspond à la variance totale issue de l'ANOVA, sans la variabilité interne
phiStar =  QUALYPSO.time$CLIMATERESPONSE$phiStar
apply(phiStar,2,sd)
sqrt(QUALYPSO.time$TOTALVAR*(1-QUALYPSO.time$DECOMPVAR[,4]))
```

On peut également représenter la tendance moyenne et l'incertitude totale associée, sous l'hypothèse que la variable de changement $Y^*(t)$ suit une loi normale de moyenne $\mu(t)$ (effet moyen global) et d'écart-type $\sqrt{Var(Y^*(t))}$ (incertitude totale).

Par défaut la fonction *plotQUALYPSOMeanChangeAndUncertainties* montre des intervalles à 90% (option *probCI* dans *listOption*). La part des différentes sources d'incertitudes dans l'épaisseur totale de la bande colorée correspond à la part dans la variance totale (voir figure précédente).

```{r}
plotQUALYPSOMeanChangeAndUncertainties(QUALYPSO.time)
```

## Analyse en fonction du niveau de réchauffement

La possibilité de considérer et de caractériser les changements attendus pour un futur correspondant à un certain réchauffement global suscite un intérêt croissant pour les scientifiques et les décideurs. Classiquement, les analyses d'incertitude sont effectuées pour différents horizons temporels. Pouvoir partitionner et quantifier les incertitudes pour un horizon de « réchauffement » donné (par exemple un horizon +2°C) permet d'apporter une plus-value importante aux multiples travaux réalisés actuellement sur cette question. Actuellement, l'approche proposée pour ce type d'analyse consiste à échantillonner les réponses climatiques pour un réchauffement global de 1.5°C et d'autres niveaux de réchauffement (Seneviratne et al. 2018; Verfaillie et al. 2018). Le package QUALYPSO propose un cadre statistique pour ce type d'analyse.

L'approche proposée ici est d'associer des niveaux de températures globales à chacune des projections à partir des températures moyennes annuelles obtenues à l'échelle planétaire avec chacun des GCM. La matrice `X_globaltas` contient ces données pour chacune des projections. Si des projections régionales obtenues avec le même GCM, les niveaux de réchauffement sont donc identiques. On prend une unique température planétaire de référence `Xref=13` par simplicité mais on peut également associer une température de référence différente pour chaque GCM, si on veut qu'elle correspond à une période/année particulière (par ex. la période pré-industrielle).

```{r}
range(X_globaltas)
dim(X_globaltas)
plot(-1,-1,xlim=range(X_time_vec),ylim=range(X_globaltas),xlab="Années",ylab="Température (°C)")
for(i in 1:nrow(X_globaltas)){
  lines(X_time_vec,X_globaltas[i,],col=i)
}

# list of options
listOption = list(typeChangeVariable='abs')

# call QUALYPSO
QUALYPSO.globaltas = QUALYPSO(Y=Y,scenAvail=scenAvail,X=X_globaltas,
                              Xfut=Xfut_globaltas,listOption=listOption)

# grand mean effect
plotQUALYPSOgrandmean(QUALYPSO.globaltas,xlab="Temperature globale (Celsius)")

# main GCM effects
plotQUALYPSOeffect(QUALYPSO.globaltas,nameEff="GCM",xlab="Temperature globale (Celsius)")

# main RCM effects
plotQUALYPSOeffect(QUALYPSO.globaltas,nameEff="RCM",xlab="Temperature globale (Celsius)")

# mean change and associated uncertainties
plotQUALYPSOMeanChangeAndUncertainties(QUALYPSO.globaltas,xlab="Temperature globale (Celsius)")

# variance decomposition
plotQUALYPSOTotalVarianceDecomposition(QUALYPSO.globaltas,xlab="Temperature globale (Celsius)")
```

# Partition des sources d'incertitude dans un ensemble multimodèle de projections climatiques transitoires"

## Contexte

.

## Objectifs du TP

Ce TP vise à appliquer le package *QUALYPSO* sur un ensemble de projections d'indicateurs hydrologiques, pour quelques bassins versants méditerranéens.

## Lecture des données

Les données sont contenues dans deux fichiers:

-   **experiences.csv**
-   **indicateurs.csv**

```{r}
# Nettoyer l'espace de travail
rm(list=ls()) 

# Si ce n'est pas déjà fait, installer le package QUALYPSO (connection internet requise)
# install.packages("QUALYPSO")

# Charger le package QUALYPSO
# lire les caractéristiques des expériences
experiences.carac = read.csv2(file = "data/experiences.csv")
nExperiences = nrow(experiences.carac)

# lire les projections
projections.raw = read.csv2(file = "data/indicateurs.csv",header = F,colClasses = "numeric",dec = ".")
vecAnnees=unlist(projections.raw[1,])
projections.all = matrix(unlist(projections.raw[2:(nExperiences+1),]),nrow=nExperiences,byrow = F)

# sélection d'un bassin versant et d'un indicateur
selectionExp = experiences.carac$BV=="Tech"&experiences.carac$indicateur=="Qann"

# Extrait informations pour QUALYPSO
Y = projections.all[selectionExp,]
scenAvail = experiences.carac[selectionExp,c(2,3,4)]

# Applique QUALYPSO
QUALYPSOOUT = QUALYPSO(Y = Y,scenAvail = scenAvail,X=vecAnnees,Xfut=1996:2099)

# moyenne d'ensemble
plotQUALYPSOgrandmean(QUALYPSOOUT)

# effet modèles climatiques
plotQUALYPSOeffect(QUALYPSOOUT,nameEff = "GCM_RCM")

# effet Hydro
plotQUALYPSOeffect(QUALYPSOOUT,nameEff = "Hydro")

# la fonction plotQUALYPSOTotalVarianceDecomposition permet d'illustrer cette décomposition
plotQUALYPSOTotalVarianceDecomposition(QUALYPSOOUT)
```
