---
title: "Outils statistiques pour les données climatiques"
author: "Thomas Opitz"
---

```{r setup, include = FALSE}
# Si nécessaire, on peut indiquer le chemin vers le répertoire de travail dans le code commenté dans ce bloc.
knitr::opts_chunk$set(echo = TRUE, # Par défault, les codes sont affichés dans le document de sortie
                      warning = FALSE, # Ne pas afficher les avertissements
                      message = FALSE, # Ne pas afficher les message de R
                      cache = TRUE,
                      out.width = "90%") 
#knitr::opts_knit$set(root.dir = '~/research/output/atelier_EC-Données-climatiques')
```

Dans ce tutoriel, nous n'avons pas de recours systématique, seulement partielle (ggplot2), aux fonctionnalités du \emph{tidyverse} (<https://www.tidyverse.org/>).

\emph{The tidyverse is an opinionated collection of R packages designed for data science. All packages share an underlying design philosophy, grammar, and data structures.}

# Préparatifs

## Charger les librairies

Chargeons d'abord les librairies nécessaires.

```{r}
library(evd) # Mean Excess Plot, treshold stability plot
library(extRemes) # extrémogramme
library(forecast) # pour visualiser la saisonnalité avec ggseasonplot
library(GeoModels) # variogramme
library(ggfortify) # fonction autoplot avec ggplot
library(ggplot2) # graphiques
# library(gstat) # geostatistique 
library(lubridate) # manipulation de dates
library(reshape2) # manipuler des données 
library(trend) # mk.test
# library(spacetime) # geostatistique spatiotemporelle
library(xts) # format et outils pour les séries chronologiques
base_size = 12 # taille de référence des polices dans ggplot
```

## Charger les données

Chargeons les données de températures pour la période historique et un modèle climatique spécifique.

```{r}
name_var = "tasmaxAdjust" # ou "prtotAdjust" pour les précipitations 
name_scenario = "Historical" # ou "rcp8.5"
name_model = "MOHC-HadGEM2-ES_CLMcom-CCLM4-8-17" # ou "IPSL-IPSL-CM5A-MR_IPSL-WRF381P"
str2add = paste0("_", name_var, "_", name_scenario, "_", name_model)
load(paste0("data/data", str2add, ".RData"))
```

Les fichiers suivants ont été chargés :

-   series, series2 : vecteur avec les séries de variables climatiques pour les pixels recouvrant (partiellement) Avignon et Écully.
-   month, year, dates : vecteurs de la même longueur que series et series2 contenant des indices temporels pour les deux séries.
-   var_array : un array contenant les données de la variable climatique pour une grille spatiale autour de Lyon et pour une période de 30 ans
-   month_sel, year_sel, time_sel, dates_sel : indices temporels de var_array
-   X_sel coordonnées x (en km) de la grille SAFRAN (Lambert II) pour var_array
-   Y_sel coordonnées y (en km) de la grille SAFRAN (Lambert II) pour var_array

Nous renommons d'abord quelques objets et les transformons vers des formats adaptés.

```{r}
var_array_tmax = var_array
series_tmax = xts(series, dates)
# On a créé un objet de type "xts" (extended time series) et "zoo" (format du package "zoo").
class(series_tmax)
series2_tmax = xts(series2, dates)
summary(series_tmax)
summary(series2_tmax)
```

De la même manière, extrayons les données de précipitations (en unité de mm, approximativement) pour le même modèle climatique et le même scénario.

```{r}
name_var = "prtotAdjust" 
str2add = paste0("_", name_var, "_", name_scenario, "_", name_model)
load(paste0("data/data", str2add, ".RData"))
var_array_rr = var_array
series_rr = xts(series, dates)
series2_rr = xts(series2, dates)
summary(series_rr)
summary(series2_rr)
```

Au cas où il y aurait des données manquantes, on peut afficher leurs dates :

```{r}
index(series)[is.na(series)]
```

Il n'y en a pas ici. A noter, la fonction "index" extrait l'indice temporel.

Ensuite, les fonctions year, month, week permettent d'obtenir l'an, le mois et la semaine de la donnée.

```{r}
head(year(index(series_tmax)))
tail(year(index(series_tmax)))
head(month(index(series_tmax)))
tail(month(index(series_tmax)))
head(week(index(series_tmax)))
tail(week(index(series_tmax)))
```

# Visualisation de la distribution

Affichons un histogramme avec ggplot. Ici, on choisit des classes de largeur "2 degrés" pour les températures.

```{r}
df = data.frame(Value = as.numeric(series_tmax))
pl = ggplot(df)+ theme_bw(base_size = base_size) + 
  xlab("Valeurs") + ylab("Comptage") +
  geom_histogram(aes(x = Value), binwidth = 2, color = "gray30",  fill = "skyblue", alpha = .75)
pl
```

Ou bien, on affiche directement des densités relativement lisses, ce qui implique une étape (implicite) de lissage des données avec un estimateur à noyau.

```{r}
pl = ggplot(df)+ theme_bw(base_size = base_size) + 
  xlab("Valeurs") + ylab("Densité") +
  geom_density(aes(x = Value), fill = "skyblue", alpha = .75)
pl
```

Nous pouvons aussi dessiner un boxplot.

```{r}
pl = ggplot(df)+ theme_bw(base_size = base_size) + 
  theme(axis.text.x=element_blank()) +
  xlab("") +
  ylab("Valeurs") +
  geom_boxplot(aes(y = Value), fill = "skyblue", alpha = .75)
pl
```

Les mêmes visualisations pour la précipitation. On retient seulement les valeurs positives ici.

```{r}
df = data.frame(Value = as.numeric(series_rr[series_rr > 0]))
pl = ggplot(df)+ theme_bw(base_size = base_size) + 
  xlab("Valeurs") + ylab("Comptage") +
  geom_histogram(aes(x = Value), binwidth = .5, , color = "gray30", fill = "orange", alpha = .75)
pl
pl = ggplot(df) + theme_bw(base_size = base_size) +
  xlab("Valeurs") + ylab("Densité") +
  geom_density(aes(x = Value), fill = "orange", alpha = .75)
pl
pl = ggplot(df)+ theme_bw(base_size = base_size) +
  theme(axis.text.x=element_blank()) +
   ylab("Valeurs") +
  xlab("") +
  geom_boxplot(aes(y = Value), fill = "orange", alpha = .75)
pl
```

Y a-t-il une différence dans les distributions entre la première et la deuxième partie de la période de simulation (avant/après 1980 - à adapter en fonction du jeu de données) ? Faisons une analyse graphique des densités.

```{r}
df = data.frame(Value = as.numeric(series_tmax))
df$year = year(dates)
df$period = ifelse(df$year <= 1980, 1, 2)
pl = ggplot(df) + theme_bw(base_size = base_size) + xlab("Valeur") + ylab("Densité") +
  geom_histogram(aes(x = Value, y = after_stat(density)), subset(df, period == 1), fill = "green", alpha = .4, binwidth = 2) +
  geom_histogram(aes(x = Value, y = after_stat(density)), subset(df, period == 2), fill = "red", alpha = .4, binwidth = 2)
pl
```

Il semble y avoir un léger décalage des valeurs entre les deux périodes.

La même analyse mais avec les densités :

```{r}
pl = ggplot(df) + theme_bw(base_size = base_size) + xlab("Valeur") + ylab("Densité") +
  geom_density(aes(x = Value), subset(df, period == 1), fill = "green", alpha = .4) +
  geom_density(aes(x = Value), subset(df, period == 2), fill = "red", alpha = .4)
pl
```

On peut répéter ces analyses pour les précipitations :

```{r}
df$Value2 = as.numeric(series_rr)
df$period = ifelse(df$year <= 1980, 1, 2)
pl = ggplot(df) + theme_bw(base_size = base_size) + xlab("Valeur") + ylab("Densité") +
  geom_histogram(aes(x = Value2, y = after_stat(density)), subset(df, period == 1), fill = "green", alpha = .4) +
  geom_histogram(aes(x = Value2, y = after_stat(density)), subset(df, period == 2), fill = "red", alpha = .4)
pl
```

Regardons maintenant le comportement des événements extrêmes en affichant le graphique de la Moyenne des Excès selon différents seuils. Nous rajoutons aussi les observations en bas du graphique.

```{r}
evd::mrlplot(series_tmax, 
             tlim = c(quantile(na.omit(series_tmax), .9), 
                      quantile(na.omit(series_tmax), .999)))
rug(series_tmax)
```

On trouve des queues de distribution relativement légères car la pente ne semble pas être positive, avec une courbe allant plutôt vers le bas, à l'incertitude près. Le même graphique, cette fois pour les précipitations :

```{r}
evd::mrlplot(series_rr, 
             tlim = c(quantile(na.omit(series_rr), .9), 
                      quantile(na.omit(series_rr), .999)))
rug(series_rr)
```

On trouve des queues de distribution plutôt lourdes car la pente semble être positive avec une courbe allant vers le haut, à l'incertitude près.

On peut aussi estimer l'indice de queue directement à partir des excès de seuils de différents niveaux.

```{r}
evd::tcplot(na.omit(series_tmax), 
            tlim = c(quantile(na.omit(series_tmax), .9), 
                     quantile(na.omit(series_tmax), .998)), model = "gpd", which = 2)
rug(series_tmax)
```

Ce graphique est difficile à interpréter ici car les valeurs estimées de l'indice de queue de distributions ne se stabilisent pas lorsque l'on augmente les seuils. On observe une tendance vers des indices de queue plus petites (et négatives) pour les seuils les plus élevés (mais l'incertitude d'estimation augmente également).

```{r}
evd::tcplot(na.omit(series_rr), 
            tlim = c(quantile(na.omit(series_rr), .9), 
                     quantile(na.omit(series_rr), .998)), model = "gpd", which = 2)
rug(series_rr)
```

Les valeurs estimées de l'indice de queue de distributions semblent se stabiliser autour d'une valeur positive à partir des seuils au delà de 3mm (à l'incertitude près).

# Analyse du comportement sériel

## Agrégation et décomposition

Regardons d'abord la seasonnalité en calculant une série de moyennes mensuelles par année. une série de moyennes mensuelles par année ?

```{r}
series_mens = aggregate(series_tmax, by = as.yearmon, FUN = mean)
head(series_mens, 24)
plot(series_mens, type = "l", xlab = "Date", ylab = "Moyenne mensuelle")
```

Une variante ggplot "automatique" est disponible pour certains types d'objets :

```{r}
autoplot(series_mens) + theme_bw(base_size = base_size) +
  ylab("Monthly series")
# On peut superposer les courbes des différentes années :
pl = ggseasonplot(ts(series_mens, 
                  start = c(year(series_mens)[1],month(series_mens)[1]), frequency = 12), 
                  year.labels=TRUE, season.labels = 1:12) +
  theme_bw(base_size = base_size) +
  xlab("Mois") + ylab("Valeur") +
  ggtitle("")
pl
```

Maintenant, faisons une décomposition en une tendance à long terme, une tendance saisonnière (périodique), et les fluctuations à court terme. La fonction "decompose" nécessite le format "ts" classique pour la série.

```{r}
decomp_tmax = decompose(ts(as.numeric(series_tmax), start = c(year(series_tmax)[1],month(series_tmax)[1]), frequency = 365.25))
autoplot(decomp_tmax) + theme_bw(base_size = base_size) + ggtitle("")
```

Nous pouvons en extraire le motif saisonnier :

```{r}
df = data.frame(Day = 1:length(decomp_tmax$figure), Value = decomp_tmax$figure)
pl = ggplot(df) + theme_bw(base_size = base_size) +
  geom_line(aes(x = Day, y = Value)) +
  geom_point(aes(x = Day, y = Value))
pl
```

Nous pouvons aussi utiliser un filtre (ici une simple moyenne mobile) pour lisser la tendance à long terme plus fortement, après avoir enlevé la saisonnalité et les fluctations à court terme :

```{r}
series_moy20 = rollmean(ts(as.numeric(series_tmax) - as.numeric(decomp_tmax$seasonal) - as.numeric(decomp_tmax$random), 
                           start = c(year(series_tmax)[1],month(series_tmax)[1]), frequency = 365.25), k = 20*365.25)
autoplot(series_moy20) + theme_bw(base_size = base_size) + 
  ylab("Smoothed trend") + ggtitle("")
```

La même analyse des séries pour les précipitations. D'abord, la série de moyennes mensuelles par année.

```{r}
series_mens = aggregate(series_rr, by = as.yearmon, FUN = mean)
head(series_mens, 12)
plot(series_mens, type = "l", xlab = "Date", ylab = "Moyenne mensuelle")
```

Superposons les courbes des différentes années :

```{r}
pl = ggseasonplot(ts(series_mens, 
                  start = c(year(series_tmax)[1], month(series_tmax)[1]), frequency = 12), 
                  year.labels=TRUE, season.labels = 1:12) +
  theme_bw(base_size = base_size) + 
  xlab("Mois") + ylab("Valeur") +
  ggtitle("")
pl
```

Faisons une décomposition en tendance à long terme, tendance saisonnière (périodique), et fluctuations à court terme.

```{r}
decomp_rr = decompose(ts(as.numeric(series_rr), start = c(year(series_rr)[1],month(series_rr)[1]), frequency = 365.25))
autoplot(decomp_rr) + theme_bw(base_size = base_size) + ggtitle("")
df = data.frame(Day = 1:length(decomp_rr$figure), Value = decomp_rr$figure)
pl = ggplot(df) + theme_bw(base_size = base_size) +
  geom_line(aes(x = Day, y = Value)) +
  geom_point(aes(x = Day, y = Value)) 
pl
```

L'aspect est moins lisse que celui du motif pour les températures, certainement en raison des queues lourdes et d'une moindre autocorrélation temporelle.

```{r}
series_moy20 = rollmean(ts(as.numeric(series_rr) - as.numeric(decomp_rr$seasonal) - as.numeric(decomp_rr$random), start = c(year(series_tmax)[1],month(series_tmax)[1]), frequency = 365.25), k = 20*365.25)
autoplot(series_moy20) + theme_bw(base_size = base_size) + ylab("Value")
```

## Analyse des auto-corrélations temporelles

Pour éviter que les variabilités saisonnière et à long terme dominent les autocorrélations, enlevons-les d'abord. Nous calculons l'autocorrélation pour un décalage maximal de 4 semaines (28 jours).

```{r}
tmp_series = na.omit(ts(as.numeric(series_tmax) - as.numeric(decomp_tmax$seasonal) - as.numeric(decomp_tmax$trend)))
acf_tmax = acf(tmp_series, 
               lag.max = 28, plot = FALSE)
autoplot(acf_tmax) + theme_bw(base_size = base_size) + ggtitle("")
```

Pour les précipitations :

```{r}
tmp_series = na.omit(ts(as.numeric(series_rr) - as.numeric(decomp_rr$seasonal) - as.numeric(decomp_rr$trend)))
acf_rr = acf(tmp_series, lag.max = 28, plot = FALSE)
autoplot(acf_rr) + theme_bw(base_size = base_size) +
  ggtitle("")
```

Qu'aurait-on obtenu sans enlever les variabilités saisonnière et à long terme ?

```{r}
autoplot(acf(series_tmax, lag.max = 28, plot = FALSE)) + theme_bw(base_size = base_size) + 
  ggtitle("")
autoplot(acf(series_rr, lag.max = 28, plot = FALSE)) + theme_bw(base_size = base_size) +
  ggtitle("")
```

On voit que les tendances saisonnière et à long terme ont une très forte influence sur la variabilité des températures, mais moins sur celle des précipitations.

Faisons la même analyse pour l'autocorrélation extrémale avec la fonction "atdf" (auto-tail dependence function). Il est utile de faire varier u pour étudier la sensibilité des résultats au choix du seuil.

```{r}
tmp_series = na.omit(ts(as.numeric(series_tmax) - as.numeric(decomp_tmax$seasonal) - as.numeric(decomp_tmax$trend)))
extremo_u1_tmax = atdf(tmp_series, u = 0.95, type = "rho", lag.max = 28)
extremo_u2_tmax = atdf(tmp_series, u = 0.98, type = "rho", lag.max = 28)
extremo_u3_tmax = atdf(tmp_series, u = 0.99, type = "rho", lag.max = 28)
df = data.frame(Décalage = extremo_u1_tmax$lag, CorrélationX = extremo_u1_tmax$atdf, Seuil = 0.95)
df = rbind(df, data.frame(Décalage = extremo_u2_tmax$lag, CorrélationX = extremo_u2_tmax$atdf, Seuil = 0.98))
df = rbind(df, data.frame(Décalage = extremo_u3_tmax$lag, CorrélationX = extremo_u3_tmax$atdf, Seuil = 0.99))
df$Seuil = factor(df$Seuil) # transformer en variable catégorique
ggplot(df) + theme_bw() +
  theme_bw(base_size = base_size) + 
  geom_point(aes(x = Décalage, y = CorrélationX, group = Seuil, color = Seuil)) +
  geom_line(aes(x = Décalage, y = CorrélationX, group = Seuil, color = Seuil)) 
```

Ici, la corrélation extrémale est moins faible que la corrélation linéaire classique, et elle décroit en augmentant le seuil. Ce comportement est indicateur de l'indépendance asymptotique temporelle.

Pour les précipitations :

```{r}
tmp_series = na.omit(ts(as.numeric(series_rr) - as.numeric(decomp_rr$seasonal) - as.numeric(decomp_rr$trend)))
extremo_u1_tmax = atdf(tmp_series, u = 0.95, type = "rho", lag.max = 14)
extremo_u2_tmax = atdf(tmp_series, u = 0.98, type = "rho", lag.max = 14)
extremo_u3_tmax = atdf(tmp_series, u = 0.99, type = "rho", lag.max = 14)
df = data.frame(Décalage = extremo_u1_tmax$lag, CorrélationX = extremo_u1_tmax$atdf, Seuil = 0.95)
df = rbind(df, data.frame(Décalage = extremo_u2_tmax$lag, CorrélationX = extremo_u2_tmax$atdf, Seuil = 0.98))
df = rbind(df, data.frame(Décalage = extremo_u3_tmax$lag, CorrélationX = extremo_u3_tmax$atdf, Seuil = 0.99))
df$Seuil = factor(df$Seuil) # transformer en variable catégorique
ggplot(df) + theme_bw() +
  theme_bw(base_size = base_size) +
  geom_point(aes(x = Décalage, y = CorrélationX, group = Seuil, color = Seuil)) +
  geom_line(aes(x = Décalage, y = CorrélationX, group = Seuil, color = Seuil)) 
```

Encore, on constate que l'autocorrélation extrémale diminue en augmentant le niveau du seuil.

Puis, nous pouvons explorer les corrélations croisées entre les composantes aléatoires des deux séries de température (Avigon / Écully).

```{r}
decomp_tmax2 = decompose(ts(as.numeric(series2_tmax), start = c(year(series2_tmax)[1],month(series2_tmax)[1]), frequency = 365.25))
tmp_series1 = na.omit(ts(as.numeric(series_tmax) - as.numeric(decomp_tmax$seasonal) - as.numeric(decomp_tmax$trend)))
tmp_series2 = na.omit(ts(as.numeric(series2_tmax) - as.numeric(decomp_tmax2$seasonal) - as.numeric(decomp_tmax2$trend)))
autoplot(ccf(tmp_series1, tmp_series2, lag.max = 14, plot = FALSE)) + 
  theme_bw(base_size = base_size) + ggtitle("")
```

La corrélation croisée est relativement forte pour des petits décalages temporels.

De même, explorons les corrélations croisées entre les composantes aléatoires des deux séries de précipitations (Avigon / Écully).

```{r}
decomp_rr2 = decompose(ts(as.numeric(series2_rr), start = c(year(series2_rr)[1],month(series2_rr)[1]), frequency = 365.25))
tmp_series1 = na.omit(ts(as.numeric(series_rr) - as.numeric(decomp_rr$seasonal) - as.numeric(decomp_rr$trend)))
tmp_series2 = na.omit(ts(as.numeric(series2_rr) - as.numeric(decomp_rr2$seasonal) - as.numeric(decomp_rr2$trend)))
autoplot(ccf(tmp_series1, tmp_series2, lag.max = 14, plot = FALSE)) + 
  theme_bw(base_size = base_size) + ggtitle("")
```

La corrélation croisée est moins forte pour les précipitations.

Enfin, explorons les corrélations croisées entre les composantes aléatoires des températures et précipitations à Avignon.

```{r}
tmp_series1 = na.omit(ts(as.numeric(series_tmax) - as.numeric(decomp_tmax$seasonal) - as.numeric(decomp_tmax$trend)))
tmp_series2 = na.omit(ts(as.numeric(series_rr) - as.numeric(decomp_rr$seasonal) - as.numeric(decomp_rr$trend)))
autoplot(ccf(tmp_series1, tmp_series2, lag.max = 14, plot = FALSE)) + 
  theme_bw(base_size = base_size) + ggtitle("")
```

On trouve une (faible) autocorrélation négative pour des décalages temporels positifs : il fait moins chaud après des précipitations importantes. Cependant, attention à l'échelle du graphique qui peut être trompeuse. Modifions-la :

```{r}
autoplot(ccf(tmp_series1, tmp_series2, lag.max = 14, plot = FALSE)) + 
  theme_bw(base_size = base_size) + ggtitle("") +
  ylim(-1,1)
```

# Tests statistiques

Nous appliquons le test de Mann-Kendall pour détecter une tendance (monotone) dans les séries. Afin d'atténuer le problème de dépendance temporelle et de grande taille d'échantillon, nous appliquons le test aux moyennes annuelles.

```{r}
series_tmax_ann = aggregate(series_tmax, by = year, FUN = mean)
autoplot(series_tmax_ann) +  theme_bw(base_size = base_size) +
   ylab("Annual mean") + ggtitle("")
mk.test(as.numeric(series_tmax_ann), alternative = "two.sided")
```

La p-value est très faible ici, et on rejette donc H_0 : il semble bien y avoir une tendance.

```{r}
series_rr_ann = aggregate(series_rr, by = year, FUN = mean)
autoplot(series_rr_ann) +  theme_bw(base_size = base_size) +
  ylab("Annual mean") + ggtitle("")
mk.test(as.numeric(series_rr_ann), alternative = "two.sided")
```

On ne rejette pas l'hypothèse d'absence de tendance monotone pour les précipitations. On peut aussi investiguer le comportement d'une mesure de dispersion, comme la variance. Y a-t-il une tendance dans les variances annuelles des fluctuations locales de la série de température ?

```{r}
series_tmax_ann = aggregate(as.zoo(decomp_tmax$random), by = year, FUN = var, na.rm = TRUE)
autoplot(series_tmax_ann) +  theme_bw(base_size = base_size) +
  ylab("Annual variance") + ggtitle("")
mk.test(as.numeric(series_tmax_ann), alternative = "two.sided")
```

Le test nous indique qu'il semble y avoir une tendance.

# Analyse spatiale

Dans cette section, on travaille avec les données sur une grille spatiale autour de Lyon.

## Cartes de résumés par pixel

Dessinons d'abord des cartes de résumés statistiques par pixel.

Par exemple : comment visualiser la moyenne des températures par pixel ?

```{r}
matrix_moy_tmax = apply(var_array_tmax, c(1,2), mean)
dim(matrix_moy_tmax)
df = reshape2:::melt(matrix_moy_tmax, value.name = "Moyenne")
df$X = X_sel[match(df$X, paste0("X", 1:length(X_sel)))]
df$Y = Y_sel[match(df$Y, paste0("Y", 1:length(Y_sel)))]
pl = ggplot(df, aes(x = X, y = Y)) + coord_fixed() +
  theme_bw(base_size = base_size) +
  ylab("Annual mean") + ggtitle("") +
  geom_tile(aes(fill = Moyenne)) + 
  scale_fill_viridis_c() +
  labs(x= "X", y= "Y") 
pl
```

Même approche pour la variance.

```{r}
matrix_var_tmax = apply(var_array_tmax, c(1,2), var)
dim(matrix_var_tmax)
df = reshape2:::melt(matrix_var_tmax, value.name = "Variance")
df$X = X_sel[match(df$X, paste0("X", 1:length(X_sel)))]
df$Y = Y_sel[match(df$Y, paste0("Y", 1:length(Y_sel)))]
pl = ggplot(df, aes(x = X, y = Y)) + coord_fixed() +
  theme_bw(base_size = base_size) +
  geom_tile(aes(fill = Variance)) + 
  scale_fill_viridis_c() +
  labs(x= "X", y= "Y") 
pl
```

Produisons les mêmes cartes pour les précipitations.

```{r}
matrix_moy_rr = apply(var_array_rr, c(1,2), mean)
dim(matrix_moy_rr)
df = reshape2:::melt(matrix_moy_rr, value.name = "Moyenne")
df$X = X_sel[match(df$X, paste0("X", 1:length(X_sel)))]
df$Y = Y_sel[match(df$Y, paste0("Y", 1:length(Y_sel)))]
pl = ggplot(df, aes(x = X, y = Y)) + coord_fixed() +
  theme_bw(base_size = base_size) +
  geom_tile(aes(fill = Moyenne)) + 
  scale_fill_viridis_c() +
  labs(x= "X", y= "Y") 
pl

matrix_var_rr = apply(var_array_rr, c(1,2), var)
dim(matrix_var_tmax)
df = reshape2:::melt(matrix_var_rr, value.name = "Variance")
df$X = X_sel[match(df$X, paste0("X", 1:length(X_sel)))]
df$Y = Y_sel[match(df$Y, paste0("Y", 1:length(Y_sel)))]
pl = ggplot(df, aes(x = X, y = Y)) + coord_fixed() +
  theme_bw(base_size = base_size) +
  geom_tile(aes(fill = Variance)) + 
  #scale_fill_gradient(low="grey90", high="red") +
  scale_fill_viridis_c() +
  labs(x= "X", y= "Y") 
pl
```

## Variogramme spatial

Calculons le variogramme spatial du champ des moyennes de températures pour une distance maximale de 150km, et construisons un objet de type ggplot ensuite.

```{r}
vario_tmax = GeoVariogram(matrix_moy_tmax, 
                          coordx = X_sel, coordy = Y_sel, 
                          maxdist = 150,
                          grid = TRUE)
# plot(vario_tmax, type = "l")
df = data.frame(Distance = vario_tmax$centers, Variogramme = vario_tmax$variograms)
pl = ggplot(df) + theme_bw(base_size = base_size) +
  geom_point(aes(x = Distance, y = Variogramme)) +
  geom_line(aes(x = Distance, y = Variogramme)) +
  labs(y= "Distance (km)") +
  labs(y= "Variogramme") 
pl
```

De même, pour le champ des moyennes de précipitations.

```{r}
vario_rr = GeoVariogram(matrix_moy_rr, 
                        coordx = X_sel, coordy = Y_sel, 
                        maxdist = 150,
                        grid = TRUE)
df = data.frame(Distance = vario_rr$centers, Variogramme = vario_rr$variograms)
pl = ggplot(df) + theme_bw(base_size = base_size) +
  geom_point(aes(x = Distance, y = Variogramme)) +
  geom_line(aes(x = Distance, y = Variogramme)) +
  labs(y= "Distance (km)") +
  labs(y= "Variogramme") 
pl
```

On peut aussi afficher les deux variogrammes dans un même graphique. Les échelles des valeurs étant très différentes entre les deux variables, nous ramenons les variogramme à la même échelle en les divisant par les variances respectives.

```{r}
df = data.frame(Distance = vario_tmax$centers, Variogramme = vario_tmax$variograms/var(as.numeric(matrix_moy_tmax)), Variable = "tmax")
df = rbind(df, data.frame(Distance = vario_rr$centers, Variogramme = vario_rr$variograms/var(as.numeric(matrix_moy_rr)), Variable = "prtot"))
pl = ggplot(df) + 
  theme_bw(base_size = base_size) +
  geom_line(aes(x = Distance, y = Variogramme, group = Variable, color = Variable), lwd = 1.5) +
  #scale_fill_gradient(low="grey90", high="red") +
  scale_fill_viridis_d() +
 labs(y= "Variogramme normalisé") 
pl
```

Aux faibles distances, la variabilité spatiale est relativement plus forte pour les moyennes des températures. En revanche, pour les moyennes de température, la variabilité spatiale augmente moins fortement aux grandes distances.

# Exercices

1.  Répéter l'analyse pour d'autres combinaisons de scénario (par exemple, RCP8.5) et de modèle climatique.
2.  Comparer les résultats entre scénarios / modèles.
